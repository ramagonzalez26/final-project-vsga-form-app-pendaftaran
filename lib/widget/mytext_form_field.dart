import 'package:flutter/material.dart';

class MyTextFormField extends StatefulWidget {
  const MyTextFormField(
      {required this.name, required this.icon, required this.onChanged});

  final Function(String?) onChanged;
  final String name;
  final Widget icon;

  @override
  State<MyTextFormField> createState() => _MyTextFormFieldState();
}

class _MyTextFormFieldState extends State<MyTextFormField> {
  @override
  Widget build(BuildContext context) {
    return TextFormField(
      onChanged: widget.onChanged,
      decoration: InputDecoration(
        border: OutlineInputBorder(),
        hintText: widget.name,
        icon: widget.icon,
      ),
    );
  }
}
