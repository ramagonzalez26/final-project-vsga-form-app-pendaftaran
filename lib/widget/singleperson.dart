import 'package:flutter/material.dart';

class SinglePerson extends StatelessWidget {
  const SinglePerson(
      {Key? key,
      required this.image,
      required this.noHp,
      required this.namaOrang});

  final String image;
  final String noHp;
  final String namaOrang;

  @override
  Widget build(BuildContext context) {
    return Card(
      child: SizedBox(
        //color: Colors.yellow,
        height: 200,
        width: 150,
        child: Column(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.symmetric(vertical: 3),
              child: Container(
                height: 150,
                width: 140,
                decoration: BoxDecoration(
                    //color: Colors.grey,
                    image: DecorationImage(
                        fit: BoxFit.fill, image: NetworkImage(image))),
              ),
            ),
            Text(
              noHp,
              style: const TextStyle(
                  fontWeight: FontWeight.bold,
                  fontSize: 17,
                  color: Color(0xff9b96d6)),
            ),
            Text(
              namaOrang,
              style: const TextStyle(
                fontSize: 17,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
