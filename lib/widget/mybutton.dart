import 'package:flutter/material.dart';

class MyButton extends StatelessWidget {
  final VoidCallback onpressed;
  final String name;
  MyButton({required this.name, required this.onpressed});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 45,
      width: double.infinity,
      child: ElevatedButton(
        onPressed: onpressed,
        style: ElevatedButton.styleFrom(backgroundColor: Colors.blueAccent),
        child: Text(name),
      ),
    );
  }
}
