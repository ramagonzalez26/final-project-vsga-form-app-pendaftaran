import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:form_app_pendaftaran/screen/data_user.dart';
import 'package:flutter/material.dart';

class DetailScreen extends StatefulWidget {
  const DetailScreen({
    required this.id,
    required this.docId,
    required this.foto,
    required this.nama,
    required this.alamat,
    required this.lokasi,
    required this.gender,
    required this.noHp,
    super.key,
  });
  final int id;
  final String docId;
  final String foto;
  final String nama;
  final String alamat;
  final String lokasi;
  final String gender;
  final String noHp;

  @override
  State<DetailScreen> createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  int count = 1;
  Widget buildSizeProduct(String name) {
    return Container(
      height: 60,
      width: 60,
      color: const Color.fromARGB(23, 16, 192, 236),
      child: Center(
        child: Text(
          name,
          style: myStyle,
        ),
      ),
    );
  }

  final TextStyle myStyle = const TextStyle(fontSize: 16, color: Colors.black);

  Widget _buildImage() {
    return Center(
      child: SizedBox(
        width: 300,
        child: Card(
          child: Container(
            padding: const EdgeInsets.all(10),
            child: Container(
              height: 220,
              decoration: BoxDecoration(
                //color: Colors.amber,
                image: DecorationImage(
                    fit: BoxFit.fill, image: NetworkImage(widget.foto)),
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget buildDetailPart() {
    return SizedBox(
      height: 200,

      //color: Colors.amber,
      child: Row(
        children: <Widget>[
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Nama\t\t\t\t\t         : ${widget.nama}",
                style: myStyle,
              ),
              Text(
                "No HP\t\t\t\t\t        : ${widget.noHp}",
                style: TextStyle(fontSize: 16, color: Color(0xff9b96d6)),
              ),
              Text(
                "Alamat\t\t\t\t\t       :  ${widget.alamat}",
                style: myStyle,
              ),
              Text(
                "Gender\t\t\t\t\t       : ${widget.gender}",
                style: myStyle,
              ),
              Text(
                "Lokasi Daftar : \n${widget.lokasi}",
                style: TextStyle(fontSize: 15, color: Colors.black),
              ),
            ],
          ),
        ],
      ),
    );
  }

  // Function to display a dialog box for editing details
  void showEditDialog() {
    TextEditingController namaController =
        TextEditingController(text: widget.nama);
    TextEditingController noHpController =
        TextEditingController(text: widget.noHp);
    String nama = widget.nama;
    String noHp = widget.noHp;
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(32.0),
          ),
          title: const Text('Edit Details'),
          content: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              TextField(
                controller: namaController,
                onChanged: (value) {
                  // Update the nama variable with the new value
                  setState(() {
                    nama = value;
                  });
                },
                decoration: const InputDecoration(labelText: 'Nama'),
              ),
              TextField(
                controller: noHpController,
                onChanged: (value) {
                  setState(() {
                    noHp = value;
                  });
                },
                decoration: const InputDecoration(labelText: 'No HP'),
              ),
              // Add more fields as needed
            ],
          ),
          actions: [
            TextButton(
              onPressed: () {
                // Close the dialog without saving changes
                Navigator.of(context).pop();
              },
              child: const Text('Cancel'),
            ),
            TextButton(
              onPressed: () {
                setState(() {
                  nama = namaController.text;
                  noHp = noHpController.text;
                });
                FirebaseFirestore.instance
                    .collection('data')
                    .doc(widget.docId)
                    .update({
                  'nama': nama,
                  'noHp': noHp,
                  // Add more fields as needed
                }).then((value) {
                  Navigator.of(context).pop();
                  Navigator.of(context).pushReplacement(
                      MaterialPageRoute(builder: (context) => DataUser()));
                });
              },
              child: const Text('Save'),
            ),
          ],
        );
      },
    );
  }

  void deleteDocument(String documentId) {
    FirebaseFirestore.instance
        .collection('data')
        .doc(documentId)
        .delete()
        .catchError((e) {})
        .whenComplete(() {
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => const DataUser()));
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "Detail Page",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon: const Icon(
            Icons.arrow_circle_left_outlined,
            color: Colors.black,
          ),
          onPressed: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => const DataUser()));
          },
        ),
        actions: <Widget>[
          IconButton(
              onPressed: () {},
              icon: const Icon(
                Icons.search,
                color: Colors.black,
              )),
          IconButton(
              onPressed: () {},
              icon: const Icon(Icons.notifications_none, color: Colors.black)),
        ],
      ),
      body: ListView(
        physics: const BouncingScrollPhysics(),
        children: <Widget>[
          _buildImage(),
          Container(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(height: 10),
                Text(
                  widget.nama,
                  style: const TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 5),
                Text(
                  widget.gender,
                  style: const TextStyle(
                    fontSize: 16,
                    color: Colors.grey,
                  ),
                ),
                const SizedBox(height: 20),
                Card(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: ListTile(
                    leading: const Icon(Icons.location_on),
                    title: Text("Lokasi Pendaftran : ${widget.lokasi}"),
                  ),
                ),
                Card(
                  margin: const EdgeInsets.symmetric(horizontal: 20),
                  child: ListTile(
                    leading: const Icon(Icons.phone),
                    title: Text(widget.noHp),
                  ),
                ),
                const SizedBox(height: 20),
                const Text(
                  'Alamat Domisili',
                  style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                const SizedBox(height: 10),
                Card(
                  child: Padding(
                    padding: const EdgeInsets.all(20.0),
                    child: ListTile(
                      leading: const Icon(Icons.location_pin),
                      title: Text(widget.alamat),
                    ),
                  ),
                ),
                const SizedBox(height: 20),
                //BUAT TOMBOL DELETE DAN EDIT DATA DARI FIRESTORE
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    ElevatedButton(
                      onPressed: () {
                        deleteDocument(
                            widget.docId); // Panggil fungsi deleteDocument(
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.red,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32.0),
                        ),
                      ),
                      child: const Text('Delete'),
                    ),
                    //Buat tombol edit juga
                    ElevatedButton(
                      onPressed: () {
                        showEditDialog();
                      },
                      style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.green,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(32.0),
                        ),
                      ),
                      child: const Text('Edit'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
