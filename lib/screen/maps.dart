import 'package:flutter/material.dart';
import 'package:form_app_pendaftaran/screen/form.dart';
import 'package:open_street_map_search_and_pick/open_street_map_search_and_pick.dart';

class MapsScreen extends StatefulWidget {
  const MapsScreen({Key? key}) : super(key: key);

  @override
  State<MapsScreen> createState() => _MapsScreenState();
}

class _MapsScreenState extends State<MapsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: OpenStreetMapSearchAndPick(
        center: LatLong(-8.50404300, 117.42849700),
        buttonColor: Colors.blue,
        buttonText: 'Pilih Lokasi',
        onPicked: (pickedData) {
          print(pickedData.latLong.latitude);
          print(pickedData.latLong.longitude);
          print(pickedData.address);
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (context) => FormScreen(
                address: pickedData.address,
              ),
            ),
          );
        },
      ),
    );
  }
}
