import 'package:form_app_pendaftaran/model/data.dart';
import 'package:form_app_pendaftaran/screen/detailscreen.dart';
import 'package:form_app_pendaftaran/screen/form.dart';
import 'package:form_app_pendaftaran/screen/listperson.dart';
import 'package:form_app_pendaftaran/widget/singleperson.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class DataUser extends StatefulWidget {
  const DataUser({Key? key}) : super(key: key);
  @override
  State<DataUser> createState() => _DataUserState();
}

late Data user1;
late Data user2;

late var dataSnap;

class _DataUserState extends State<DataUser> {
  List<String> documentIds = [];

  Widget _buildLihatData() {
    return SizedBox(
      height: 50,
      //color: Colors.blue,
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 50,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    const Text(
                      "Data",
                      style:
                          TextStyle(fontSize: 17, fontWeight: FontWeight.bold),
                    ),
                    GestureDetector(
                      onTap: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => ListPerson(
                                  nama: "Data Semua",
                                  mySnapshot: dataSnap,
                                  documentIds: documentIds,
                                )));
                      },
                      child: const Text(
                        "Lihat Semua",
                        style: TextStyle(
                          fontSize: 17,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget __buildCard2User() {
    return Row(
      children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Row(
              children: <Widget>[
                if (user1.exists)
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => DetailScreen(
                          id: user1.id,
                          docId: user1.docId,
                          foto: user1.foto,
                          nama: user1.nama,
                          alamat: user1.alamat,
                          lokasi: user1.lokasi,
                          gender: user1.gender,
                          noHp: user1.noHp,
                        ),
                      ));
                    },
                    child: SinglePerson(
                      image: user1.foto,
                      noHp: user1.noHp,
                      namaOrang: user1.nama,
                    ),
                  ),
                if (user2.exists)
                  GestureDetector(
                    onTap: () {
                      Navigator.of(context).pushReplacement(MaterialPageRoute(
                        builder: (context) => DetailScreen(
                          id: user2.id,
                          docId: user2.docId,
                          foto: user2.foto,
                          nama: user2.nama,
                          alamat: user2.alamat,
                          lokasi: user2.lokasi,
                          gender: user2.gender,
                          noHp: user2.noHp,
                        ),
                      ));
                    },
                    child: SinglePerson(
                      image: user2.foto,
                      noHp: user2.noHp,
                      namaOrang: user2.nama,
                    ),
                  ),
              ],
            )
          ],
        ),
      ],
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Beranda",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        centerTitle: true,
        elevation: 0.0,
        backgroundColor: Colors.grey[100],
      ),
      body: FutureBuilder<QuerySnapshot>(
          future: FirebaseFirestore.instance.collection("data").get(),
          builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
            dataSnap = snapshot;

            if (snapshot.connectionState == ConnectionState.waiting ||
                snapshot.data == null ||
                dataSnap == null) {
              return const Center(
                child: CircularProgressIndicator(),
              );
            }
            // Clear the documentIds list before populating it again
            documentIds.clear();

            // Iterate through the documents and store only the document IDs
            for (DocumentSnapshot document in snapshot.data!.docs) {
              String documentId = document.id;
              documentIds.add(documentId);
            }

            if (snapshot.hasData) {
              user1 = Data(
                id: 0,
                docId: documentIds.length > 0 ? documentIds[0] : "",
                foto: documentIds.length > 0
                    ? snapshot.data!.docs[0]["foto"]
                    : "",
                nama: documentIds.length > 0
                    ? snapshot.data!.docs[0]["nama"]
                    : "",
                noHp: documentIds.length > 0
                    ? snapshot.data!.docs[0]["noHp"]
                    : "",
                lokasi: documentIds.length > 0
                    ? snapshot.data!.docs[0]["lokasi"]
                    : "",
                alamat: documentIds.length > 0
                    ? snapshot.data!.docs[0]["alamat"]
                    : "",
                gender: documentIds.length > 0
                    ? snapshot.data!.docs[0]["gender"]
                    : "",
                exists: documentIds.length >
                    0, // Set the flag based on document existence
              );

              user2 = Data(
                id: 1,
                docId: documentIds.length > 1 ? documentIds[1] : "",
                foto: documentIds.length > 1
                    ? snapshot.data!.docs[1]["foto"]
                    : "",
                nama: documentIds.length > 1
                    ? snapshot.data!.docs[1]["nama"]
                    : "",
                noHp: documentIds.length > 1
                    ? snapshot.data!.docs[1]["noHp"]
                    : "",
                lokasi: documentIds.length > 1
                    ? snapshot.data!.docs[1]["lokasi"]
                    : "",
                alamat: documentIds.length > 1
                    ? snapshot.data!.docs[1]["alamat"]
                    : "",
                gender: documentIds.length > 1
                    ? snapshot.data!.docs[1]["gender"]
                    : "",
                exists: documentIds.length >
                    1, // Set the flag based on document existence
              );
            } else {
              user1 = Data(
                id: 0,
                docId: "",
                foto: "",
                nama: "",
                noHp: "",
                lokasi: "",
                alamat: "",
                gender: "",
                exists: false, // Set the flag to false as data is not available
              );

              user2 = Data(
                id: 1,
                docId: "",
                foto: "",
                nama: "",
                noHp: "",
                lokasi: "",
                alamat: "",
                gender: "",
                exists: false, // Set the flag to false as data is not available
              );
            }

            return Container(
              height: double.infinity,
              width: double.infinity,
              margin: const EdgeInsets.symmetric(horizontal: 20),
              child: ListView(
                children: <Widget>[
                  Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        SizedBox(
                          height: 500,
                          width: double.infinity,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              _buildLihatData(),
                              user1.exists || user2.exists
                                  ? __buildCard2User()
                                  : Container(
                                      padding: EdgeInsets.only(
                                          top: MediaQuery.of(context)
                                                  .size
                                                  .height /
                                              4),
                                      alignment: Alignment.center,
                                      child: const Column(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Icon(
                                            Icons.image_not_supported_outlined,
                                            size: 80,
                                            color: Colors.grey,
                                          ),
                                          SizedBox(height: 20),
                                          Text(
                                            "No Data Available",
                                            style: TextStyle(
                                                fontSize: 18,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ],
                                      ),
                                    ),
                            ],
                          ),
                        ),
                      ]),
                ],
              ),
            );
          }),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushReplacement(
            context,
            MaterialPageRoute(
                builder: (context) => FormScreen(
                      address: "",
                    )),
          );
        },
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }
}
