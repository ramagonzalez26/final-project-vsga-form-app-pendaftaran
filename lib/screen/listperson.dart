import 'package:form_app_pendaftaran/screen/detailscreen.dart';
import 'package:form_app_pendaftaran/screen/data_user.dart';
import 'package:form_app_pendaftaran/widget/singleperson.dart';
import 'package:flutter/material.dart';

class ListPerson extends StatelessWidget {
  const ListPerson(
      {Key? key,
      required this.nama,
      required this.mySnapshot,
      required this.documentIds})
      : super(key: key);

  final String nama;
  final mySnapshot;
  final List<String> documentIds;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          "List Page",
          style: TextStyle(
              color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0.0,
        leading: IconButton(
          icon:
              const Icon(Icons.arrow_circle_left_outlined, color: Colors.black),
          onPressed: () {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => const DataUser()));
          },
        ),
      ),
      body: Container(
        margin: const EdgeInsets.symmetric(horizontal: 20),
        child: ListView(
          children: [
            Column(
              children: [
                SizedBox(
                  height: 50,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Text(
                            nama,
                            style: const TextStyle(
                                fontSize: 17, fontWeight: FontWeight.bold),
                          ),
                        ],
                      )
                    ],
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),
                SizedBox(
                  height: 550,
                  width: 400,
                  child: GridView.builder(
                    itemCount: mySnapshot.data.docs.length,
                    itemBuilder: (context, index) => GestureDetector(
                      onTap: () {
                        Navigator.of(context).pushReplacement(MaterialPageRoute(
                            builder: (context) => DetailScreen(
                                id: index,
                                docId: documentIds[index],
                                foto: mySnapshot.data.docs[index]["foto"],
                                nama: mySnapshot.data.docs[index]["nama"],
                                alamat: mySnapshot.data.docs[index]["alamat"],
                                lokasi: mySnapshot.data.docs[index]["lokasi"],
                                gender: mySnapshot.data.docs[index]["gender"],
                                noHp: mySnapshot.data.docs[index]["noHp"])));
                      },
                      child: SinglePerson(
                          image: mySnapshot.data.docs[index]["foto"],
                          noHp: mySnapshot.data.docs[index]["noHp"],
                          namaOrang: mySnapshot.data.docs[index]["nama"]),
                    ),
                    scrollDirection: Axis.vertical,
                    gridDelegate:
                        const SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 1,
                      crossAxisSpacing: 10,
                      mainAxisSpacing: 10,
                      childAspectRatio: 1.5,
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }
}
