import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';

import 'package:form_app_pendaftaran/model/model_data.dart';
import 'package:form_app_pendaftaran/screen/data_user.dart';
import 'package:form_app_pendaftaran/screen/maps.dart';
import 'package:form_app_pendaftaran/widget/mybutton.dart';
import 'package:open_street_map_search_and_pick/open_street_map_search_and_pick.dart';

import '../widget/mytext_form_field.dart';

class FormScreen extends StatefulWidget {
  FormScreen({Key? key, required this.address}) : super(key: key);

  final String address;

  @override
  State<FormScreen> createState() => _FormScreenState();
}

class _FormScreenState extends State<FormScreen> {
  String radioButtonItem = 'Pria';

  // Group Value for Radio Button.
  int gender = 1;
  String _jeniskelamin = '';
  String _alamat = '', genders = '', nama = '';
  String _foto = '', _noHp = '', lokasi = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text(
            "Form Pendaftaran",
            style: TextStyle(
                color: Colors.black, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          centerTitle: true,
          elevation: 0.0,
          backgroundColor: Colors.grey[100],
          leading: IconButton(
            icon: Icon(
              Icons.arrow_circle_left_outlined,
              color: Colors.black,
            ),
            onPressed: () {
              Navigator.of(context).pushReplacement(
                  MaterialPageRoute(builder: (context) => DataUser()));
            },
          ),
        ),
        body: Form(
          child: ListView(
            children: [
              Container(
                height: 600,
                width: 500,
                margin: EdgeInsets.symmetric(horizontal: 10),
                padding: EdgeInsets.only(top: 10),
                // color: Colors.black,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      height: 550,
                      width: double.infinity,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          MyTextFormField(
                            name: "Nama",
                            icon: Icon(Icons.account_box),
                            onChanged: (value) {
                              setState(() {
                                nama = value!;
                              });
                            },
                          ),
                          MyTextFormField(
                            name: "Alamat",
                            icon: const Icon(Icons.location_on),
                            onChanged: (value) {
                              setState(() {
                                _alamat = value!;
                              });
                            },
                          ),
                          MyTextFormField(
                            name: "No. HP",
                            icon: const Icon(Icons.phone_android),
                            onChanged: (value) {
                              setState(() {
                                _noHp = value!;
                              });
                            },
                          ),
                          Column(
                            children: <Widget>[
                              Row(
                                mainAxisAlignment: MainAxisAlignment.start,
                                children: <Widget>[
                                  const Text('Jenis Kelamin :',
                                      style: TextStyle(fontSize: 18)),
                                  Radio(
                                    value: 1,
                                    groupValue: gender,
                                    onChanged: (val) {
                                      setState(() {
                                        radioButtonItem = 'Pria';
                                        gender = 1;
                                        _jeniskelamin = 'Pria';
                                      });
                                    },
                                  ),
                                  const Text(
                                    'Pria',
                                    style: TextStyle(fontSize: 17.0),
                                  ),
                                  Radio(
                                    value: 2,
                                    groupValue: gender,
                                    onChanged: (val) {
                                      setState(() {
                                        radioButtonItem = 'Wanita';
                                        gender = 2;
                                        _jeniskelamin = 'Wanita';
                                      });
                                    },
                                  ),
                                  const Text(
                                    'Wanita',
                                    style: TextStyle(
                                      fontSize: 17.0,
                                    ),
                                  ),
                                ],
                              ),
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            children: <Widget>[
                              const Text('Pilih Lokasi :',
                                  style: TextStyle(fontSize: 18)),
                              const SizedBox(
                                width: 10,
                              ),
                              ElevatedButton(
                                onPressed: () {
                                  Navigator.of(context).push(MaterialPageRoute(
                                      builder: (context) => MapsScreen()));
                                },
                                style: ElevatedButton.styleFrom(
                                  backgroundColor: Colors.transparent,
                                ),
                                child: Text("pilih"),
                              ),
                            ],
                          ),
                          TextFormField(
                            initialValue: widget.address,
                            onChanged: (value) {
                              setState(() {
                                lokasi = widget.address;
                              });
                            },
                            decoration: const InputDecoration(
                              border: OutlineInputBorder(),
                              hintText: "Lokasi Pendaftaran",
                              icon: Icon(Icons.location_searching),
                            ),
                          ),
                          MyTextFormField(
                            name: "Link Foto",
                            icon: const Icon(Icons.camera_alt),
                            onChanged: (value) {
                              setState(() {
                                _foto = value!;
                              });
                            },
                          ),
                          MyButton(
                              name: "Submit",
                              onpressed: () {
                                Navigator.of(context).pushReplacement(
                                    MaterialPageRoute(
                                        builder: (context) => DataUser()));
                                postDetailsToFirestore();
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }

  Widget _pickLocation() {
    return OpenStreetMapSearchAndPick(
        center: LatLong(23, 89),
        buttonColor: Colors.blue,
        buttonText: 'Set Current Location',
        onPicked: (pickedData) {
          print(pickedData.latLong.latitude);
          print(pickedData.latLong.longitude);
          print(pickedData.address);
        });
  }

  postDetailsToFirestore() async {
    // calling our firestore
    // calling our user model
    // sedning these values

    FirebaseFirestore firebaseFirestore = FirebaseFirestore.instance;
    User? user = FirebaseAuth.instance.currentUser;
    DataModel dataModel = DataModel();

    // writing all the values

    dataModel.nama = nama;
    dataModel.alamat = _alamat;
    dataModel.noHp = _noHp;
    dataModel.gender = _jeniskelamin;
    dataModel.lokasi = widget.address;
    dataModel.foto = _foto;

    await firebaseFirestore
        .collection("data")
        .doc(user?.uid)
        .set(dataModel.toMap());
    Fluttertoast.showToast(msg: "Akun berhasil dibuat :) ");
  }
}
