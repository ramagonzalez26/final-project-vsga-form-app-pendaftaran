//try import of dart:io
//try import of dart:io

class Data {
  final int id;
  final String docId;
  final String nama;
  final String alamat;
  final String noHp;
  final String gender;
  final String lokasi;
  final String foto;
  final bool exists;

  Data({
    required this.id,
    required this.docId,
    required this.nama,
    required this.alamat,
    required this.noHp,
    required this.gender,
    required this.lokasi,
    required this.foto,
    this.exists = true,
  });
}
