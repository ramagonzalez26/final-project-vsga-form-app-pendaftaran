class DataModel {
  String? nama;
  String? alamat;
  String? noHp;
  String? gender;
  String? lokasi;
  String? foto;

  DataModel({
    this.nama,
    this.alamat,
    this.noHp,
    this.gender,
    this.lokasi,
    this.foto,
  });

  // receiving data from server
  factory DataModel.fromMap(map) {
    return DataModel(
      nama: map['nama'],
      alamat: map['alamat'],
      noHp: map['noHp'],
      gender: map['gender'],
      lokasi: map['lokasi'],
      foto: map['foto'],
    );
  }

  // sending data to our server
  Map<String, dynamic> toMap() {
    return {
      'nama': nama,
      'alamat': alamat,
      'noHp': noHp,
      'gender': gender,
      'lokasi': lokasi,
      'foto': foto,
    };
  }
}
